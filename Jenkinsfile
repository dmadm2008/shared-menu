#!/usr/bin/env groovy

def nodeLabel = "master"
def repo = [
    url: 'https://bitbucket.org/dmadm2008/shared-menu.git',
    jenkins_creds_id: ''
]
def local_repo_dir = 'repository'
def version_files = [ "version.release", "version.patch", "version.build" ]

def build_info = [
    version: []
]

node( nodeLabel ) {
    parameters([
        string( name: "stages", defaultValue: "parameters", trim: true ),
        string( name: "run_level", defaultValue: "1", trim: true ),
        string( name: "patch_branch", defaultValue: "", trim: true ),
    ])

    stage( "Process Parameters" ) {
        println params
        cleanWs()
        if ( ! hasTheStageBeenRequested( "init", params.stages )) { return }
        echo "Hello"
    }

    stage( "Check Version" ) {
        if ( ! hasTheStageBeenRequested( "version", params.stages )) { return }
        echo "Version check stage"
    }
    stage( "Clone Repo" ) {
        if ( ! hasTheStageBeenRequested( "clone_repo", params.stages )) { return }
        cloneRepo([
            repo_url: repo.url,
            jenkins_creds_id: repo.jenkins_creds_id,
            branches: [ 'master', 'version2-01' ],
            dest_dir: local_repo_dir
        ])

        dir( local_repo_dir ) {
            checkoutBranch( 'version2-01', false )
            sh("git branch -a")
            build_info.version = readVersion( version_files )
            sh("pwd; ls -l")
            println build_info
        }
    }
    stage( "Merge to Master" ) {
        if ( ! hasTheStageBeenRequested( "merge", params.stages )) { return }
        dir( local_repo_dir ) {
            println "Merging to master branch"
            checkoutBranch( 'master', false )
            sh("git branch -a")
            def master_branch_version = readVersion( version_files )
            if ( hasVersionChanged( master_branch_version, build_info.version )) {
                println "Reset build number to 0"
                writeFile([ text: "0", file: "version.build" ])
            } else {
                println "Version is not changed"
            }
        }
    }
}

return

def hasTheStageBeenRequested( String stage, String stages ) {
    return stages.split(',').contains( stage )
}

def checkoutBranch( String branch, boolean is_new=false ) {
    sh("git checkout " + (( is_new == true ) ? " -b " : " ") + branch)
}

def currentBranch() {
    return sh([ script: "git rev-parse --abbrev-ref HEAD", returnStdout: true ])
}

/**
 * Returns a list of version numbers
 * from the provided list of files
 */
def readVersion( List version_files ) {
    def version = []
    version_files.each{ file ->
        readFile( file ).split('\\.').each{ num -> version << num.trim() }
    }
    return version
}

def hasVersionChanged( List old_v, new_v ) {
    println "Old version: " + old_v
    println "New version: " + new_v
    def sum_new = 0
    for ( int i = 0; i < new_v.size(); i++ ) {
        sum_new += new_v[i] << i*8
    }
    def sum_old = 0
    for ( int i = 0; i < old_v.size(); i++ ) {
        sum_old += old_v[i] << i*8
    }
    println "Final sums. Old: ${sum_old}, new: ${sum_new}"
    return sum_old > sum_new
}

/**
 * Clones a repo with specified branches
 * Params:
 * - dest_dir - where to clone the repo
 * - repo_url - url of repo
 * - jenkins_creds_id - jenkins credentials for the repo
 * - branches - a list of branches to checkout
 */
def cloneRepo( Map args ) {
    def branches = []
    args.branches.each{ it ->  branches << [ name: 'origin/' + it ]}
    println "Selected branches: " + branches
    checkout([
        changelog: false,
        poll: false,
        scm: [
            $class: 'GitSCM',
            branches: branches,
            doGenerateSubmoduleConfigurations: false,
            extensions: [
                [$class: 'RelativeTargetDirectory', relativeTargetDir: args.dest_dir],
                [$class: 'CleanBeforeCheckout']
            ],
            submoduleCfg: [],
            userRemoteConfigs: [
                [url: args.repo_url, credentialsId: args.jenkins_creds_id]
            ]
        ]
    ])
}



