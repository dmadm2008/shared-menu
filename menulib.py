#!/usr/bin/env python

import os
import argparse

class CLIMenuOption(object):
    def __init__(self, *args, **kwargs):
        self.name = kwargs.get('name')
        self.desc = kwargs.get('desc')
        self.menu_group = kwargs.get('menu_group')
        self.default_value = kwargs.get('default_value')
        self.value = kwargs.get('value')
        self.cli_arg = kwargs.get('cli_arg')
        self.az_cli_arg = kwargs.get('az_cli_arg')

class CLIMenuOptions(object):
    def __init__(self):
        self.__options = []

    def add(self, option):
        assert(type(option)) == CLIMenuOption
        self.__options.append(option)

    def set(self, options):
        self.__options = options

    @property
    def options(self):
        return self.__options


class CLIMenuBuilder(object):
    """Renders cli menu"""
    def __init__(self, *args, **kwargs):
        cli_options = kwargs.get('menu_options', None) # pylint: disable=missing-docstring
        assert(cli_options) != None

        self.__options = CLIMenuOptions()
        self.__options.set(cli_options)

        self.__env_prefix = kwargs.get('env_prefix', '')
        assert(type(self.__options)) is CLIMenuOptions

        title = kwargs.get('title', '')
        arg_parser = kwargs.get('parser', None)
        self.__parser = argparse.ArgumentParser(title) if arg_parser is None else arg_parser

    def build_menu(self):
        for option in self.__options.options:
            env_name = (self.__env_prefix + option.name).upper()
            env_value = os.environ.get(env_name, None)
            help_text = '{2} (Env: {0}, defaut: {1})'.format(env_name, env_value, option.desc)
            self.__parser.add_argument(
                option.cli_arg,
                default=env_value,
                help=help_text,
            )

    @property
    def parser(self):
        return self.__parser

    def args(self):
        return self.__parser.parse_args()



if __name__ == "__main__":
    pass
