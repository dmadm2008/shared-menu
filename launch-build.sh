#!/bin/bash

set -x

# bash ./validate-Jenkinsfile.sh
git add . ; git commit -am 'Autocommit'; git push origin version2-01

if [ $? -ne 0 ]; then
    echo " >>> Jenkinssfile did not validate"
    exit 1
else
    echo " >>> Jenkinsfile validated"
fi

JENKINS_URL=http://192.168.3.10:8080

parameters=$(<config.json)

status_code=$(curl -k -X POST --data-urlencode json="$parameters" -s -u "${JENKINS_API_USER}:${JENKINS_API_TOKEN}" -w "%{http_code}" $JENKINS_URL/job/shared-menu/build)
# status_code=$(curl -k -X POST --form json="$parameters" -s -u "${JENKINS_API_USER}:${JENKINS_API_TOKEN}" -w "%{http_code}" $JENKINS_URL/job/shared-menu/build)

if [ x"$status_code" == x"201" ]; then
    echo " >>> Job launched"
    exit 0
else
    echo " >>> Job launch failed with code: ${status_code}"
    exit 1
fi

