#!/usr/bin/env python
"""
Sample tool to demonstrate the menulib.py library
"""

from menulib import CLIMenuOption as Option
from menulib import CLIMenuBuilder as CLIBuilder


CONFIG_OPTIONS = (
    Option(
        name='resource_group',
        desc='Name of resource group',
        cli_arg='--resource-group',
        az_cli_arg='--resource-group',
    ),
    Option(
        name='subscription',
        desc='Azure subscription',
        cli_arg='--subscription',
        az_cli_arg='--subscription',
    ),
    Option(
        name='region',
        desc='Azure region',
        cli_arg='--region',
        az_cli_arg='--location',
    ),
    Option(
        name='tags',
        desc='Tags to assing on a resource group when create',
        cli_arg='--tags',
        az_cli_arg='--tags',
    ),
)


def cli_menu(): # pylint: disable=missing-docstring
    menu_builder = CLIBuilder(
        menu_options=CONFIG_OPTIONS,
        title='Manage Azure resource groups',
        env_prefix='ARO_',
    )
    menu_builder.parser.add_argument(
        '--action', default='config',
        choices=('config', 'create', 'delete'), help='Select an action to perform'
    )
    menu_builder.build_menu()
    args = menu_builder.args()
    print(args.__dict__) # pylint: disable=superfluous-parens

    if args.action == 'create':
        print("Need to create something") # pylint: disable=superfluous-parens


if __name__ == "__main__":
    cli_menu()
